//= modernizr.custom.js
//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/owl.carousel/dist/owl.carousel.min.js'
//= ../../node_modules/slick-carousel/slick/slick.min.js
//= ../../node_modules/jquery.maskedinput/src/jquery.maskedinput.js
//= jquery.dlmenu.js

$(document).ready(function() {
	$('.owl-carousel-slider').owlCarousel({
		loop:true,
		smartSpeed: 1000,
		nav:true,
		dots:true,
		dotsEach:true,
		navText:false,
		items: 1,
		autoplay: true,
	});
	$('.owl-carousel-brand').owlCarousel({
		loop:true,
		smartSpeed: 1000,
		nav:true,
		dots:false,
		dotsEach:true,
		navText:true,
		items: 4,
		autoplay: true,
		responsive:{
			0:{
				items:1
			},
			1000:{
				items:4
			}
		}
	});
	$('.owl-carousel-interest').owlCarousel({
		loop:true,
		smartSpeed: 1000,
		nav:true,
		dots:false,
		dotsEach:true,
		navText:true,
		items: 4,
		autoplay: true,
		responsive:{
			0:{
				items: 1
			},
			639: {
				items: 3
			},
			950:{
				items: 4
			}
		}
	});

	$('.product-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.product-slider__nav'
	});
	$('.product-slider__nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.product-slider',
		dots: false,
		centerMode: false,
		focusOnSelect: true
	});
	//product__slider-nav


	sizeDropMenu();

	var countText = 0;
	$(window).resize(function(e) {
		sizeDropMenu();
		if ($(window).width() < 639) {
			if (!$('div').is('.read-all') && countText == 0) {
				$('.text-info > p:first-child').append('<span class="read-all">Читать полностью</span>');
				countText++;
			}
		}
	});

	if ($(window).width() < 639) {
		$('.text-info > p:first-child').append('<span class="read-all">Читать полностью</span>');
		countText++;
	}

	$('body').on('click', '.read-all', function(){
		$('.text-info > p').show();
		$('.text-info').find('.read-all').remove();
	});

	$('.mob-menu__item > span').on('click', function() {
		$('.mob-menu__item').removeClass('active');
		$(this).parent().addClass('active');
	});

	$( '#dl-menu' ).dlmenu({});

	$('.mob-menu__close').on('click', function(e) {
		$('.mob-menu__item').removeClass('active');
	});

	$('.popup-btn_call').on('click', function() {
		popupShow($('.popup_callback'));
	})
	$('.popup-btn_write').on('click', function() {
		popupShow($('.popup_write'));
	})

	$('.popup__close').on('click', function() {
		popupClose();
	});

	$('.popup_callback-write').on('click', function() {
		$('.popup').fadeOut();
		$('.popup_write').fadeIn();
	});

	$('.popup_write-callback').on('click', function() {
		$('.popup').fadeOut();
		$('.popup_callback').fadeIn();
	});

	$('body').on('click', '.popup-back', function(){
		popupClose();
	});

	$('.product__options li').on('click', function() {
		if ( !$(this).hasClass('none') ) {
			$('.product__options li').removeClass('select');
			$(this).addClass('select');
		}
	});

	$('#order-phone').mask('+7 (999) 999 99 99');

	$('#order-file').change(function(e) {
		var label	 = $(this).next(),
			 labelVal = label.html(),
			 fileName = '',
			 size = 0;


			if( $(this)[0].files && $(this)[0].files.length > 1 ) {
				fileName = ( $(this).attr( 'data-multiple-caption' ) || '' ).replace( '{count}', $(this)[0].files.length );
				for (var i = 0; i < $(this)[0].files.length; i++) {
					size += $(this)[0].files[i].size;
				}
				size = (size / 1024 / 1024).toFixed(2);
			}
			else {
				size = ($(this)[0].files[0].size / 1024 / 1024).toFixed(2);
				fileName = e.target.value.split( '\\' ).pop();
				label.addClass('change');
			}

			if( fileName ) {
				label.find( 'span' ).text(fileName);
				label.find( 'span' ).html(fileName + '<span class="order-size"> (' + size + 'Мб)</span>' );
			}
			else
				label.text(labelVal);
	});

	var arr = $('.retail__item .product__features-val');
	Array.prototype.forEach.call(arr, function(el) {
		var wrap = el.parentNode,
			elStyle = getComputedStyle(el)
			
		wrap.style.height = elStyle.height;
	});
});

function sizeDropMenu() {
	var wMenu = $('.menu').width();
	var hMenuDropdown = $('.menu__dropdown').css('height');

	$('.menu__dropdown-second').css('width', wMenu-290 + 'px');
	$('.menu__dropdown-second').css('height', hMenuDropdown);
}

function popupShow(el) {
	el.fadeIn();
	$('body').append('<div class="popup-back"></div>');
}

function popupClose() {
	$('.popup').fadeOut();
	setTimeout(function() {
		$('body').find('.popup-back').remove();
	}, 250);
}
